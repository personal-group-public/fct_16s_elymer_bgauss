@echo off
cd production_logs

:: Check for changes before committing
:loop
git add .
git commit -m "logs_%TIME%" || echo "No changes to commit."

:: Set upstream branch if not already set
git push --set-upstream origin production_default || echo "Push failed or branch already set."

:: Wait for a short period
timeout 3

:: Wait for the specified duration before repeating
timeout 3000 /nobreak
goto loop
